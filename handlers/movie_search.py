from copy import deepcopy

from telebot.types import Message

from config_data import (
    messages,
    config,
)
from loader import bot

from states.movie_information import MovieInfoState
from site_API.core import (
    headers,
    site_api,
    url,
)

from database.main_db import store_request_params
from keyboards.inline import menu_kb

show_kb = menu_kb.show_back_to_main_keyboard()


def create_message_to_send(**data) -> str:
    api_response = site_api.get_movie_by_name()
    response = api_response(**data)
    if response:
        print(response)
        try:
            movie_info = response["docs"][0]

            name = movie_info["name"]
            year = movie_info["year"]
            countries = [i_country["name"] for i_country in movie_info["countries"]]
            genres = [i_genre["name"] for i_genre in movie_info["genres"]]
            rating = movie_info["rating"]["kp"]
            description = movie_info["description"]
            link = f'https://www.kinopoisk.ru/film/{movie_info["id"]}/'

            text_to_send = (
                f'{name} ({year})\n'
                f'Рейтинг Kinopoisk: {rating}/10\n'
                f'Жанр: {", ".join(genres)}\n'
                f'Страны: {", ".join(countries)}\n'
                f'\n{description}\n'
                f'\n{link}\n'
            )
        except IndexError as exc:
            print(type(exc))
            text_to_send = "Нет данных о фильме с таким названием"

        return text_to_send


@bot.message_handler(commands=['movie_search'])
def search_movie(message: Message) -> None:
    """
    Обработчк команды поиска по названию
    """
    bot.set_state(message.from_user.id, MovieInfoState.name, message.chat.id)
    bot.send_message(
        chat_id=message.from_user.id,
        text=messages.search_by_name_message
    )


@bot.message_handler(state=MovieInfoState.name)
def get_name(message: Message) -> None:
    movie_search_params = deepcopy(config.MOVIE_SEARCH_PARAMS)
    movie_search_params["query"] = message.text

    # Запись запроса в базу данных
    store_request_params(
        input_user_name=message.from_user.username,
        input_message=messages.db_msg_name
    )

    text_to_send = create_message_to_send(
        method="GET",
        url=url,
        headers=headers,
        params=movie_search_params)

    bot.send_message(
        chat_id=message.from_user.id,
        text=text_to_send,
        reply_markup=show_kb
    )
    bot.delete_state(message.from_user.id, message.chat.id)

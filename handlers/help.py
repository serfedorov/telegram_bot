from telebot.types import Message
from loader import bot
from config_data.config import DEFAULT_COMMANDS

from keyboards.inline import menu_kb

show_kb = menu_kb.show_back_to_main_keyboard()


@bot.message_handler(commands=['help'])
def bot_help(message: Message) -> None:
    """
    Обработчик команды /help
    """
    text_to_send = handle_bot_help()
    bot.send_message(
        chat_id=message.from_user.id,
        text=text_to_send,
        reply_markup=show_kb
    )


def handle_bot_help() -> str:
    """
    Функция формирует справку: список из команд в config.py по запросу /help
    """
    help_list = [f"/{command} - {desk}" for command, desk in DEFAULT_COMMANDS]
    return "\n".join(help_list)

import re
from copy import deepcopy
from typing import Any

from telebot.types import Message

from config_data import (
    messages,
    config,
)
from database.main_db import store_request_params
from loader import bot

from states.movie_information import MovieInfoState
from site_API.core import (
    headers,
    site_api,
    url,
)

from keyboards.inline import menu_kb

show_kb = menu_kb.show_back_to_main_keyboard()


def check_input(usr_range: str) -> Any:
    """
    Функция проверки на корректность ввода
    :param usr_range: ввод пользователя
    :return: диапазон рейтинга и вариант сортировки
    """
    range_output = ''
    sorting = 1
    try:
        result = re.match(r'\b\d{1,2}[.]?\d{0,3}-\d{1,2}[.]?\d{0,3}', usr_range)

        if result is None:
            raise IOError('Неверный формат ввода')
        else:
            range_list = [float(i) for i in usr_range.split("-")]

            for i in range_list:
                if i < 0.0 or i > 10.0:
                    raise ValueError(f'Число {i} вне диапазона')
            if range_list[0] > range_list[1]:
                range_list = [range_list[1], range_list[0]]
                sorting = -1
            range_output = f'{range_list[0]}-{range_list[1]}'
            print(range_output)
    except (IOError, ValueError) as exc:
        print(exc)
    return range_output, sorting


def create_message_to_send(**data) -> str:
    api_response = site_api.get_movie_by_rating()
    response = api_response(**data)
    if response:
        movies_list = response["docs"]
        text_to_send = ''

        for i_position, i_movie in enumerate(movies_list):
            text_to_send += f'{i_position + 1}. {i_movie["name"]} ({i_movie["year"]}) kp:{i_movie["rating"]["kp"]}/10 \n'
        return text_to_send


@bot.message_handler(commands=['movie_by_rating'])
def search_by_rating(message: Message) -> None:
    """
    Обработчик команды поиска по рейтингу
    """
    bot.set_state(message.from_user.id, MovieInfoState.rating, message.chat.id)
    bot.send_message(
        chat_id=message.from_user.id,
        text=messages.search_by_rating_message
    )


@bot.message_handler(state=MovieInfoState.rating)
def get_rating(message: Message) -> None:
    search_range, search_sorting = check_input(message.text)
    if not search_range:
        bot.send_message(
            chat_id=message.from_user.id,
            text="Ошибка ввода диапазона! Попробуйте еще раз\n"
        )
    else:
        with bot.retrieve_data(message.from_user.id, message.chat.id) as data:
            data["sort_from_low_to_high"] = search_sorting
            data["rating"] = search_range

        bot.set_state(message.from_user.id, MovieInfoState.limit, message.chat.id)
        bot.send_message(
            chat_id=message.from_user.id,
            text=messages.limit_message
        )


@bot.message_handler(state=MovieInfoState.limit)
def get_limit(message: Message) -> None:

    # Запись запроса в базу данных
    store_request_params(
        input_user_name=message.from_user.username,
        input_message=messages.db_msg_rating
    )

    if message.text.isdigit():
        with bot.retrieve_data(message.from_user.id, message.chat.id) as data:
            data["limit"] = message.text

        movie_search_params = deepcopy(config.MOVIE_SEARCH_PARAMS)
        movie_search_params["sort_from_low_to_high"] = data["sort_from_low_to_high"]
        movie_search_params["rating"] = data["rating"]
        movie_search_params["limit"] = data["limit"]

        text_to_send = create_message_to_send(
            method="GET",
            url=url,
            headers=headers,
            params=movie_search_params)

        bot.send_message(
            chat_id=message.from_user.id,
            text=text_to_send,
            reply_markup=show_kb
        )
        bot.delete_state(message.from_user.id, message.chat.id)
    else:
        bot.send_message(message.from_user.id, "Неверный ввод! Введите число!")

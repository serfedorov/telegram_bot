from telebot.types import Message
from loader import bot

from database.main_db import retrieve_history
from keyboards.inline import menu_kb

show_kb = menu_kb.show_back_to_main_keyboard()


def handle_request_history() -> str:
    last_requests = retrieve_history()
    return last_requests


@bot.message_handler(commands=['history'])
def get_history(message: Message) -> None:
    """
    Обработчик команды Показать историю запросов
    :param message:
    :return:
    """
    text_to_send = handle_request_history()
    bot.send_message(
        chat_id=message.from_user.id,
        text=text_to_send,
        reply_markup=show_kb
    )


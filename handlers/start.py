from telebot.types import Message, CallbackQuery
from loader import bot

from keyboards.inline import menu_kb
from config_data import messages
from states.movie_information import MovieInfoState
from handlers.movie_by_budget import handle_movies_by_budget
from handlers.help import handle_bot_help
from handlers.history import handle_request_history


main_kb = menu_kb.show_start_keyboard()
search_kb = menu_kb.show_search_keyboard()
budget_kb = menu_kb.show_budget_keyboard()
back_kb = menu_kb.show_back_to_main_keyboard()


@bot.message_handler(commands=['start'])
def send_welcome(message: Message) -> None:
    """
    Обработчик команды /start
    """
    bot.send_message(
        chat_id=message.chat.id,
        text=messages.welcome_message,
        reply_markup=main_kb,
    )


@bot.message_handler(commands=['cancel'])
def send_welcome(message: Message) -> None:
    """
    Обработчик команды /cancel
    """
    bot.delete_state(message.from_user.id, message.chat.id)
    bot.send_message(
        chat_id=message.chat.id,
        text=messages.search_message,
        reply_markup=search_kb
    )


@bot.callback_query_handler(func=lambda call: call.data)
def main_callback_query(call: CallbackQuery) -> None:
    """
    Функция отлавливания нажатой кнопки меню
    """
    if call.data == "search":
        bot.answer_callback_query(call.id)
        bot.edit_message_text(
            chat_id=call.message.chat.id,
            message_id=call.message.message_id,
            text=messages.search_message,
            reply_markup=search_kb
        )

    elif call.data == "name":
        bot.set_state(call.message.chat.id, MovieInfoState.name, call.message.chat.id)
        bot.edit_message_text(
            chat_id=call.message.chat.id,
            message_id=call.message.message_id,
            text=messages.search_by_name_message
        )

    elif call.data == "budget":
        bot.answer_callback_query(call.id)
        bot.edit_message_text(
            chat_id=call.message.chat.id,
            message_id=call.message.message_id,
            text=messages.search_by_budget_message,
            reply_markup=budget_kb
        )

    elif call.data == "high_budget" or call.data == "low_budget":
        bot.answer_callback_query(call.id)
        budget_type = call.data
        result = handle_movies_by_budget(budget_type, call.message.chat.username)
        bot.edit_message_text(
            chat_id=call.message.chat.id,
            message_id=call.message.message_id,
            text=result,
            reply_markup=back_kb
        )

    elif call.data == "rating":
        bot.answer_callback_query(call.id)
        bot.set_state(call.message.chat.id, MovieInfoState.rating, call.message.chat.id)
        bot.edit_message_text(
            chat_id=call.message.chat.id,
            message_id=call.message.message_id,
            text=messages.search_by_rating_message
        )

    elif call.data == "history":
        bot.answer_callback_query(call.id)
        text_to_send = handle_request_history()
        bot.send_message(
            chat_id=call.message.chat.id,
            text=text_to_send,
            reply_markup=back_kb
        )

    # elif call.data == "survey":
    #     bot.answer_callback_query(call.id)
    #     bot.send_message(
    #         chat_id=call.message.chat.id,
    #         text=messages.survey_message
    #     )

    elif call.data == "help":
        bot.answer_callback_query(call.id)
        help_text = handle_bot_help()
        bot.edit_message_text(
            chat_id=call.message.chat.id,
            message_id=call.message.message_id,
            text=f'{messages.help_message}\n{help_text}'
        )

    elif call.data == "main_menu":
        bot.answer_callback_query(call.id)
        bot.edit_message_text(
            chat_id=call.message.chat.id,
            message_id=call.message.message_id,
            text=messages.main_message,
            reply_markup=main_kb
        )


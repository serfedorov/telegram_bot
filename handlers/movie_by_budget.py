from copy import deepcopy

from telebot.types import Message

from database.main_db import store_request_params
from loader import bot

from keyboards.inline import menu_kb
from config_data import messages, config
from site_API.core import headers, site_api, url


def create_message_to_send(**data) -> str:
    api_response = site_api.get_movie_by_budget()
    response = api_response(**data)

    if response:
        movies_list = response["docs"]
        result_text = ''

        for i_position, i_movie in enumerate(movies_list):
            result_text += f'{i_position + 1}. {i_movie["name"]} \n'
        return result_text


def get_movies_list(sorting: int) -> str:
    movie_search_params = deepcopy(config.MOVIE_SEARCH_PARAMS)
    movie_search_params["sort_from_low_to_high"] = sorting

    text_to_send = create_message_to_send(
        method="GET",
        url=url,
        headers=headers,
        params=movie_search_params)
    return text_to_send


def handle_movies_by_budget(callback: str, usr_name: str) -> str:
    sort_from_low_to_high = 1
    budget_type = ''
    if callback == "high_budget":
        sort_from_low_to_high = -1
        budget_type = messages.high_budget_movie_message

    elif callback == "low_budget":
        sort_from_low_to_high = 1
        budget_type = messages.low_budget_movie_message

    response_from_api = get_movies_list(sort_from_low_to_high)

    # Запись запроса в базу данных
    store_request_params(
        input_user_name=usr_name,
        input_message=messages.db_msg_budget
    )
    return f'{budget_type} \n\n{response_from_api}'


@bot.message_handler(commands=['movie_by_budget'])
def search_by_budget(message: Message):
    """
    Обработчик команды поиска по бюджету
    """
    store_request_params(
        input_user_name=message.from_user.username,
        input_message=messages.db_msg_budget
    )
    budget_kb = menu_kb.show_budget_keyboard()
    bot.send_message(message.chat.id, "ПОИСК ФИЛЬМОВ ПО БЮДЖЕТУ", reply_markup=budget_kb)

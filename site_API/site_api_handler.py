import json
from typing import Dict

import requests


def _make_response(method: str, url: str, headers: Dict, success=200):
    response = requests.request(method, url, headers=headers, timeout=15)
    if response.status_code == success:
        json_dict = json.loads(response.text)
        return json_dict
    else:
        return response.status_code


def _get_movie_by_name(method: str, url: str, headers: Dict,
                       params: Dict, func=_make_response):
    """
    Функция поиска информации по имени фильма/сериала
    :param method:
    :param url:
    :param headers:
    :param params:
    :param func:
    :return:
    """
    query = params["query"]

    url = f'{url}/search?page=1&limit=1&query={query}'
    response = func(method, url, headers=headers)

    return response


def _get_movie_by_budget(method: str, url: str, headers: Dict,
                         params: Dict, func=_make_response):
    """
    Функция получения списка высокобюджетных или низкобюджетных фильмов
    :param method:
    :param url:
    :param headers:
    :param params:
    :param func:
    :return:
    """
    limit = params["limit"]
    sort_from_low_to_high = params["sort_from_low_to_high"]

    url = (f"{url}?page=1&limit={limit}&selectFields=name&selectFields=budget&notNullFields=name&notNullFields=budget"
           f".value&sortField=budget.value&sortType={sort_from_low_to_high}&budget.value=%210")

    response = func(method, url, headers=headers)
    return response


def _get_movie_by_rating(method: str, url: str, headers: Dict,
                         params: Dict, func=_make_response):
    """
    Функция получения списка фильмов по рейтингу
    :param method:
    :param url:
    :param headers:
    :param params:
    :param func:
    :return:
    """
    limit = params["limit"]
    sort_from_low_to_high = params["sort_from_low_to_high"]
    rating_from = params["rating"]

    url = (f"{url}?page=1&limit={limit}&selectFields=name&selectFields=year&selectFields=rating"
           f"&notNullFields=name&sortField=rating.kp&sortType={sort_from_low_to_high}"
           f"&rating.kp={rating_from}")

    response = func(method, url, headers=headers)
    return response


class SiteApiInterface:

    @staticmethod
    def get_movie_by_name():
        return _get_movie_by_name

    @staticmethod
    def get_movie_by_budget():
        return _get_movie_by_budget

    @staticmethod
    def get_movie_by_rating():
        return _get_movie_by_rating


if __name__ == '__main__':
    _make_response()
    _get_movie_by_name()
    _get_movie_by_rating()
    _get_movie_by_budget()

    SiteApiInterface()

import json
import requests

from config_data.config import SiteSettings
from site_API.site_api_handler import SiteApiInterface

site = SiteSettings()
url = site.HOST_API

headers = {
    "accept": "application/json",
    "X-API-KEY": site.RAPID_API_KEY
}
site_api = SiteApiInterface()

if __name__ == "__main__":
    site_api()


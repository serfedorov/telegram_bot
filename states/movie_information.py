from telebot.handler_backends import StatesGroup, State


class MovieInfoState(StatesGroup):
    name = State()
    budget = State()
    rating = State()
    sorting = State()
    limit = State()


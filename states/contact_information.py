from telebot.handler_backends import StatesGroup, State


class UserInfoState(StatesGroup):
    name = State()
    age = State()
    country = State()
    city = State()
    phone_numbers = State()

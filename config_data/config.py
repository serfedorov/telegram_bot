import os
from dotenv import load_dotenv, find_dotenv
from pydantic import BaseConfig, SecretStr, StrictStr
#from pydantic_settings import BaseSettings


if not find_dotenv():
    exit("Переменные окружения не загружены т.к отсутствует файл .env")
else:
    load_dotenv()


class SiteSettings(BaseConfig):
    RAPID_API_KEY: SecretStr = os.getenv("SITE_API_KEY", None)
    HOST_API: StrictStr = os.getenv("HOST_API", None)


class BotSettings(BaseConfig):
    BOT_TOKEN: SecretStr = os.getenv("BOT_TOKEN", None)


DEFAULT_COMMANDS = (
    ("start", "Запустить бота"),
    ("help", "Вывести справку"),
    # ("survey", "Опрос"),
    ("movie_by_budget", "Поиск фильмов/сериалов по бюджету"),
    ("movie_search", "Поиск фильма/сериала по названию"),
    ("movie_by_rating", "Поиск фильмов/сериалов по рейтингу"),
    ("history", "Просмотр истории запросов и поиска фильма/сериала")
)

MOVIE_SEARCH_PARAMS = {
    "query": None,
    "page": "1",
    "limit": "10",
    "sort_from_low_to_high": 1,
    "rating": "8-8.5"
}



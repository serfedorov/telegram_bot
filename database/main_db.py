from database.common.models import (
    db,
    History,
)
from database.core import crud

db_write = crud.create()
db_read = crud.retrieve()


def store_request_params(input_user_name, input_message) -> None:
    """
    Функция сохранения события в базу данных
    :param input_user_name: логин пользователя
    :param input_message: название события для записи в базу данных
    :return:
    """
    data = [
        dict(db_usrName=input_user_name,
             db_criterias=input_message)
    ]
    db_write(db, History, data)


def retrieve_history() -> str:
    """
    Функция получения 10 последних запросов из базы данных
    :return:
    """
    retrieved = db_read(db, History, History.created_at, History.db_usrName, History.db_criterias)
    event_list = list()

    for element in retrieved:
        event = ''
        event += str(element.created_at) + ' ' + element.db_usrName + ' ' + element.db_criterias + '\n'
        event_list.append(event)

    result = [f"{i_event}" for i_event in event_list]

    if len(result) > 10:    # вывод последних 10 событий
        result = result[-10:]

    return "\n".join(result)


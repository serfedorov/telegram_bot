from datetime import datetime

import peewee as pw

db = pw.SqliteDatabase('history_requests.db')


class ModelBase(pw.Model):
    created_at = pw.DateField(default=datetime.now())

    class Meta():
        database = db


class History(ModelBase):

    db_usrName = pw.TextField()
    db_criterias = pw.TextField()

import telebot
from telebot.storage import StateMemoryStorage
from config_data.config import BotSettings

storage = StateMemoryStorage()
bot = telebot.TeleBot(token=BotSettings.BOT_TOKEN, state_storage=storage)

from telebot.types import (
    InlineKeyboardMarkup,
    InlineKeyboardButton,
)


def show_start_keyboard() -> InlineKeyboardMarkup:
    """
    Функция отображает стартовую клавиатуру
    :return:
    """
    start_keyboard = InlineKeyboardMarkup()
    start_keyboard.add(InlineKeyboardButton("Поиск фильмов/сериалов", callback_data="search"))
    start_keyboard.add(InlineKeyboardButton("История запросов", callback_data="history"))
    # start_keyboard.add(InlineKeyboardButton("Опрос", callback_data="survey"))
    start_keyboard.add(InlineKeyboardButton("Помощь", callback_data="help"))
    return start_keyboard


def show_search_keyboard() -> InlineKeyboardMarkup:
    """
    Функция отображает клавиатуру для выбора вариантов поиска
    :return:
    """
    search_keyboard = InlineKeyboardMarkup()
    search_keyboard.add(InlineKeyboardButton("По названию", callback_data="name"))
    search_keyboard.add(InlineKeyboardButton("По рейтингу", callback_data="rating"))
    search_keyboard.add(InlineKeyboardButton("По бюджету", callback_data="budget"))
    search_keyboard.add(InlineKeyboardButton("Главное меню", callback_data="main_menu"))
    return search_keyboard


def show_budget_keyboard() -> InlineKeyboardMarkup:
    """
    Функция отображает клавиатуру для выбора высоко/низко бюджетных фильмов
    :return:
    """
    budget_keyboard = InlineKeyboardMarkup()
    budget_keyboard.add(InlineKeyboardButton("Высоко бюджетные", callback_data="high_budget"))
    budget_keyboard.add(InlineKeyboardButton("Низко бюджетные", callback_data="low_budget"))
    budget_keyboard.add(
        InlineKeyboardButton("Назад", callback_data="search"),
        InlineKeyboardButton("Главное меню", callback_data="main_menu")
    )
    return budget_keyboard


def show_back_to_main_keyboard() -> InlineKeyboardMarkup:
    """
    Функция отображает клавиатуру для возврата в главное меню
    :return:
    """
    back_to_main_keyboard = InlineKeyboardMarkup()
    back_to_main_keyboard.add(InlineKeyboardButton("Главное меню", callback_data="main_menu")
    )
    return back_to_main_keyboard
